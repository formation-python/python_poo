import json


class WeatherInfo:

    def __init__(self,**kwargs):

        for key, values in kwargs.items():
            setattr(self, key, values)


weather_infos_list = []

with open("regression_lineaire/meteo_2009.json") as json_file:
    json_list = json.load(json_file)
    for month in json_list:
        weather = WeatherInfo(**month)
        weather_infos_list.append(weather)

for weather in weather_infos_list:
    print(weather.Humidité)
