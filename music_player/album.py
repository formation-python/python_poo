from decorators import print_decorator

class Album:
    def __init__(self, title, year):
        self.title = title
        self.year = year
        self.music_list = []
        self.playlist_list = []

    def add_playlist(self, playlist):
        self.playlist_list.append(playlist)

    @print_decorator
    def add_music(self, music):
        self.music_list.append(music)
        music.set_album(self)