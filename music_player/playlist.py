from decorators import print_decorator


class Playlist :
    def __init__(self, title, creation_date,picture_url):
        self.title = title
        self.creation_date = creation_date
        self.picture_url = picture_url
        self.music_list = []
        self.album_list = []

    @print_decorator
    def add_music(self, music):
        self.music_list.append(music)
        music.add_playlist(playlist=self)

    def add_album(self, album):
        self.music_list.append(album)
        album.add_playlist(playlist=self)

    def remove_music(self, music):
        self.music_list.remove(music)
        music.remove_playlist(self)

