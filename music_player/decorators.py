
def print_decorator(f):
    def new_function(*args, **kwargs):
        print(f"Vous avez ajouté le titre {args[1].title}")
        res = f(*args, **kwargs)
        return res
    return new_function
