from playlist import Playlist
from music import  Music
from album import  Album
from artist import Artist


#relation Playlist / Music
my_music = Music("Nothing else matters", 6, 1809)
print(my_music.year)
my_new_playlist = Playlist("My Playlist here again",2021,"https://random/picture.jpg")
my_new_playlist.add_music(my_music)
print(my_new_playlist.music_list[0].title)
print(my_music.playlist_list[0].title)

my_new_playlist.remove_music(my_music)
print("la liste des titre dans ma playlist :")
print(my_new_playlist.music_list)


#Relation Music / Album
album = Album("Back to black", 2000)
music2 = Music("The Unforgiven", 6, 2002)
album.add_music(my_music)
album.add_music(music2)
print(album.music_list)
print(music2.album.title)


#Relation Artist / Music

my_artist = Artist(full_name="Metallica",birth_date=1985)
music2 = Music("Mama said", 6, 2002, my_artist)

print(music2.artist.full_name)
print(my_artist.music_list[0].title)

