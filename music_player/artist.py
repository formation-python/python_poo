class Artist:
    def __init__(self,full_name, birth_date="", picture_url="", bio=""):
        self.full_name = full_name
        self.birth_date = birth_date
        self.picture_url = picture_url
        self.bio = bio
        self.music_list = []

    def add_music(self,music):
        self.music_list.append(music)

