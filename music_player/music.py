class Music:
    def __init__(self, title,duration,year,artist=None):
        self.title = title
        self.duration = duration
        self.year = year
        self.artist = artist
        self.album = None
        self.playlist_list = []
        self.is_playing = False

    @property
    def artist(self):
        return self.__artist

    @artist.setter
    def artist(self, artist):
        if artist is not None:
            self.__artist = artist
            artist.add_music(self)

    @property
    def year(self):
        return self.__year

    @year.setter
    def year(self,value):
        if value<1900 or value>2021:
            self.__year = 1900
            print("Vous avez choisi une année < 1900 ou > 2021")
        else:
            self.__year = value

    def play(self):
        self.is_playing = True

    def stop(self):
        self.is_playing = False

    def add_playlist(self, playlist):
        self.playlist_list.append(playlist)

    def remove_playlist(self,playlist):
        self.playlist_list.remove(playlist)

    def set_album(self,album):
        self.album = album







