import json

import numpy as np

class MyLinearRegression:

    def __init__(self):
        self.coef_ = None #Coefficient dirr
        self.intercept_ = None #ordonnée à l'origine

    def fit(self, X, y):

        if len(X.shape) == 1:
            X = X.reshape(-1, 1)
        X = np.c_[np.ones(X.shape[0]), X]
        xTx = np.dot(X.T, X)
        inverse_xTx = np.linalg.inv(xTx)
        xTy = np.dot(X.T, y)
        coef = np.dot(inverse_xTx, xTy)

        # set attributes theta
        self.intercept_ = coef[0]
        self.coef_ = coef[1:]


    def predict(self, X):
        # check if X is 1D or 2D array
        if len(X.shape) == 1:
            X = X.reshape(-1, 1)
        return self.intercept_ + np.dot(X, self.coef_)




X_data = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12])
y_target = np.array([3.4765482319763956, 3.127108025694152, 1.845325488375832, 2.7167984409290273, 3.0738432838361636, 2.3132706879279925, 2.8720944139715745, 2.7830979285951303, 2.7239851984252876, 2.622555426595908, 3.6673061694417903, 4.103894810577786, 2.8746020654737006, 4.004805769475413, 3.5514156474793808, 3.6741732250986785, 2.7773425619139163, 3.4984059087115917, 4.263502284671324, 2.8095460778086068, 3.000665142149774, 3.133542641948279, 4.272876422286672, 5.4742699268398844, 4.759139003770609, 5.070377368723405, 4.178233913886426, 4.914667265848286, 3.9682224033032822, 4.469697940773567, 4.670683777193973, 4.510797232092334, 4.897494226612935, 4.552282188956348, 5.564141102118783, 4.932820529192371, 5.049165011239198, 4.630815791195875, 5.534657158826535, 4.407230562060335, 5.896827287229808, 5.323541746272507, 4.989833279348231, 5.245033291398572, 5.066287468244359, 5.401885165827527, 5.330193471884493, 5.699479832190234, 5.842918512168612, 5.438896788071357, 6.208027183078591, 6.3866287726147934, 5.916609215345072, 6.624234899698185, 6.700248254415582, 6.338143526276707, 7.580488280952882, 6.742905292979038, 6.159057881557623, 6.017362558274821, 6.79130288270226, 6.638472631218706, 7.562957793961768, 6.527296590822842, 6.876952432997629, 6.6228423150341, 7.516123117981717, 6.118640816990627, 6.717414488142582, 6.853828592168121, 7.804252447194512, 7.884507886218847, 7.500829838947516, 8.183974742677606, 7.801642795048465, 9.022314022487855, 6.767977349993328, 7.975054919919253, 7.030600852554688, 7.170717530468013, 9.448936312492476, 9.312705373966901, 7.270974594348286, 8.197102740345592, 8.286078128826228, 7.1147537516120165, 8.295726788376042, 8.546433736673176, 8.446600364630479, 7.192611876494518, 9.651705342039413, 9.162299839319642, 8.9885423483981, 8.59619608018566, 9.427319492776583, 9.544295276197312, 8.750303861211671, 9.074441130462128, 9.492545799063276, 7.915457023080346, 9.790486025045492, 8.642369519002346, 9.200598175923297, 9.990222865610743, 9.642023989031342, 9.497004055807611, 9.556058615221996, 10.204804299229188, 10.12464429953963, 9.277504891507107, 10.590205611572477, 10.157168146480227, 11.787339725310417, 10.004903272832967, 10.246740372877444, 10.226298591189863, 10.175187664325614, 10.504115802250853, 10.57892738400084, 9.506397721695922, 10.799394255129327, 10.375663245955003, 11.825259857894663, 12.522158468278036, 11.908622641824344, 11.320654514072967, 11.704100207640682, 11.218923553297518, 12.063685558912946, 11.503030529312474])


mlr = MyLinearRegression()
mlr.fit(X_data, y_target)
theta0 = mlr.intercept_
theta1 = mlr.coef_

predictions = mlr.predict(np.array([8,12,0]))
print(predictions)




#utilisation des données météorologique pour prédire les precipitations :

#traitement des données
#construction des vecteurs :

#parcourir tous les fichiers Json :
weather_data = []
for i in range(2009,2021):
    with open(f'meteo_{i}.json') as json_file:
        weather_data.append(json.load(json_file))
weather_data= np.array(weather_data)
print(weather_data)


years = []
months= []
av_tem=[]
wind_speed=[]
day_duration = []
precipitation = []
for year in range(len(weather_data)):
    for month in weather_data[year]:
        years.append(year+2009)
        months.append(int(month["month"]))
        av_tem.append(float(month["Température moyenne"][:-1]))
        wind_speed.append(int(month["Vitesse du vent"][:-4]))
        day_duration_minute = (int(month["Durée du jour"].split(":")[0])*60)+int(month["Durée du jour"].split(":")[1])
        day_duration.append(int(day_duration_minute))
        precipitation.append(float(month["Précipitations moyennes par jour"][:-2]))



weather_lr = MyLinearRegression()
print(np.array(np.c_[av_tem, wind_speed, day_duration]))
print(precipitation)
weather_lr.fit(np.array(np.c_[av_tem, wind_speed, day_duration]),precipitation)
predictions = weather_lr.predict(np.array([[25,20,800]]))
print(predictions)
print(weather_lr.coef_)
print(weather_lr.intercept_)