from classroom.person import Person


class Professor(Person):

    def add_course(self, course):
        self.courses.append(course)
        course.set_professor(self)

    def show_courses(self):
        for course in self.courses:
            print(course.title)

    def add_notes_to_student(self, note, student):
        note.set_professor(self)
        note.set_student(student)
        self.notes.append(note)

    def __init__(self, full_name="", bio="", age=0, sign_presence=False):
        Person.__init__(self,full_name,bio,age,sign_presence)
        self.courses = []
        self.notes = []
