class Note:
    def __init__(self, course, date="2021-04-01", mark=0, commentary=""):
        self.date = date
        self.mark = mark
        self.commentary = commentary
        self.professor = None
        self.student = None
        self.course = course

    def set_student(self, student):
        self.student = student
        student.add_notes(self)

    def set_professor(self,professor):
        self.professor = professor
