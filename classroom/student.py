from classroom.person import Person


class Student(Person):
    def inscrire(self, cours):
        self.courses.append(cours)
        cours.add_student(self)

    def show_enrolled_courses(self):
        for course in self.courses:
            print(course.title)

    def add_notes(self, note):
        self.notes.append(note)


    def __init__(self, full_name="", bio="", age=0, level="", sign_presence=False):
        self.level = level
        self.courses=[]
        self.notes = []
        Person.__init__(self,full_name,bio,age,sign_presence)

