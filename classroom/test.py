from course import Course
from note import Note
from professor import Professor
from student import Student
import gc


my_course = Course(file="", title="Python")
#1
print(my_course.title)
print(my_course.duration)
'''
my_note = Note()
professor = Professor(full_name="John Doe")
student = Student(full_name="Alex Student")
print(professor.full_name)
print(student.full_name)
print(my_note.mark)

my_course.professor = professor
print("*********")
print(my_course.title)
print(my_course.professor.full_name)
'''
#list de chaines de caracteres
names_list= ["Alex","Fred","Amelie","Marc","John"]
ages_list= [23,29,16,25,30]

#liste d'objects Student
students_list = []

#creer une liste d'eleves
for i in range(len(names_list)):
    students_list.append(Student(full_name=names_list[i], age=ages_list[i]))

print(students_list)

print("add students to course")

my_course.students = students_list


my_course.show_students_names()
print(my_course.average_age())


my_new_lesson = Course()
#2
print(my_new_lesson.COURSES_NUMBER)

lesson_3 = Course()
#3
print(lesson_3.COURSES_NUMBER)


#Test methodes inscrire

course_2 = Course(file="", title="Python")
course_3 = Course(title="Java")
student_2 = Student(full_name="Alex Student", age=22)
student_3 = Student(full_name="Fred Maths", age=22)
student_4 = Student(full_name="François Java", age=22)



print(course_2.students)

student_2.inscrire(course_2)
student_2.inscrire(course_3)

student_3.inscrire(course_3)
student_4.inscrire(course_2)

print("étudiants inscrits au cours Python")
course_2.show_students_names()

print("étudiants inscrits au cours Java")
course_3.show_students_names()

print("Liste des cours pour Alex")
student_2.show_enrolled_courses()

#ajouter des cours à un professur
print("cours pour professeur")
professor_2 = Professor(full_name="John Doe")
professor_2.add_course(course_2)
professor_2.add_course(course_3)
print(professor_2.show_courses())
print(course_3.professor.full_name)


#ajout des notes :
professor_2.add_notes_to_student(Note(course=course_2,mark=14),student_2)


print("get students info")
print(student_2.full_name)
print(student_2.notes[0].mark)

#sign presence method
professor_2.sign_presence()
print(professor_2.is_present)

student_2.sign_presence()
print(student_2.is_present)
