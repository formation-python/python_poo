

class Course:

    COURSES_NUMBER = 0

    def activate(self):
        self.is_active = True

    def deactivate(self):
        self.is_active = False

    def show_students_names(self):
        for student in self.students:
            print(student.full_name)

    def average_age(self):
        length = len(self.students)
        sum = 0
        for student in self.students:
            sum = sum+student.age
        mean = sum/length
        return mean

    def add_student(self,student):
        self.students.append(student)

    def set_professor(self,professor):
        self.professor = professor


    @classmethod
    def increment_courses_number(cls):
        cls.COURSES_NUMBER+=1



    def __init__(self,file="", title="",is_active=False,duration=2,summary="",start_date="",end_date=""):
        self.increment_courses_number()
        self.title = title
        self.is_active = is_active
        self.duration = duration
        self.summary = summary
        self.start_date = start_date
        self.end_date = end_date
        self.file = file
        self.students = []
        self.professor = None




