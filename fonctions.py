import json
import my_script as fonctions
from my_script import ma_fonction_affichage
import requests


def message():
    print("ceci est un message")


def calcul_carre_return(reel):
    return reel**2


def calcul_produit(x, y):
    return x*y


def calcul_puissance(x, y):
    return x**y


def calculatrice(x, y, operation='unknown'):
    if operation == "produit":
        return calcul_produit(x, y)
    elif operation == "puissance":
        return calcul_puissance(x, y)
    else:
        return "l'operation n'est pas reconnue"


message()
"*****"
fonctions.calcul_carre(2)
"****"
resultat = calcul_carre_return(3)
print("le resultat de la fonction est " + str(resultat))
print(calcul_produit(2, 8))

resultat2 = calculatrice(3, 5, "puissance")
print(resultat2)

# arguments nommés :
resultat3 = calculatrice(x=3, y=5)
print(resultat3)

# args


ma_fonction_affichage(
    age=30, ville="Lyon", prenom="Youssef", nom="El Behi")

# kwargs


def display(string):
    print(string)


# operations fichiers
'''
animal_file = open("animaux.txt", 'r')
lines = animal_file.readlines()
for line in lines:
    display(line)

animal_file.close()
'''


with open("animaux.txt", 'r+') as animal_file:
    lines = animal_file.readlines()

    print(lines)
    for line in lines:
        display(line)
    animal_file.write("\nun autre animal")


# modes : r => read : lecture seule
# w => ecrire + ecraser
# a => ajouter à la fin du fichier
# r+ => ecriture et lecture

with open("animaux2.txt", 'a') as f:
    f.write("\nLion")


# recuperer un fichier depuis un url
animals = requests.get(
    "https://gitlab.com/formation-python/python_poo/-/raw/master/animaux2.txt")
data = animals.text

for i, animal in enumerate(data.split('\n')):
    print(f'animal {i+1} : {animal}')


# json files

animal = {
    "nom": "Girafe",
    "poids": 1200,
    "carateristics": {
        "nom": ['Girafe', 'Vache'],
        "matricule": 12
    }
}

print(animal)

with open("animal.json", "w") as f:
    json.dump(animal, f, indent=4)


'''
1. 
Ecrire une fonction qui calcul le max de deux nombres: 
en utilisant la fonciton précedante, calculer le max de 3 nombres

2. ecrire une fonction qui inverse une chaine de caracteres (utiliser la boucle while)

3. ecrire une fonction qui calcul le factoriel d'un nombre (positif)
n! = n*(n-1)*(n-2)...*1 
n=0 n! = 1

4. ecrire une fonction retourne un dictionnaire contenant le nombre de lettre minuscules et majuscules dans un string
result={"maj":3, "min":22}

5. ecrire une fonction qui calcul le nombre d'apparition de chaque lettre d'un string
exemple : {'a': 1, '3': 1, 'r': 2, 'e': 2, 'x': 1, 'o': 1, 'u': 1, 'c': 1}


6. ecrire une fonction qui verifie qu'un string est un palindrome ou pas:
Palindrome : Mot ou groupe de mots qui peut se lire indifféremment de gauche à droite ou de droite à gauche en gardant le même sens (ex. la mariée ira mal ; Roma Amor).

'''


def max_2(x, y):
    if x > y:
        return x
    return y


print(max_2(5, 2))
print(max_2(3, max_2(5, 6)))


def max_3(x, y, z):
    return max_2(x, max_2(y, z))


print(max_3(2, 7, 2))


def invert_word(string):
    new_word = ''
    index = len(string)

    while index > 0:
        new_word = new_word + string[index - 1]
        index = index - 1
    return new_word


print(invert_word("Youssef"))


def factoriel(n):
    if n == 0:
        print(n)
        return 1
    else:
        print(n)
        return n * factoriel(n - 1)


print(factoriel(5))


def i_do_nothing():
    pass


def dict_maj(word):
    result = {"maj": 0, "min": 0}

    for c in word:
        if c.isupper():
            result["maj"] += 1
        elif c.islower():
            result["min"] += 1
        else:
            pass
    return result


print(dict_maj("This Is My Word"))


def count_car(word):
    my_result = {}

    for c in word:
        if c in my_result:
            my_result[c] += 1
        else:
            my_result[c] = 1

    return (my_result)


print(count_car("another word"))


def check_palindrome(word):
    pos_g = 0
    pos_d = len(word) - 1

    while pos_d >= pos_g:
        if word[pos_g] != word[pos_d]:
            return False
        pos_d -= 1
        pos_g += 1
    return True


print(check_palindrome("roma amPr"))
