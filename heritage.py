class Mere:
    def bonjour(self):
        return "la classe Mere vous dit Bonjour"

class Fille(Mere):
    def salut(self):
        return "Un salut de classe Fille"


f_1 = Fille()
print(f_1.salut())
print(f_1.bonjour())


#classe mere
class Transport:
    def __init__(self,capacity = 0, constructor="", speed=0):
        self.capacity = capacity
        self.constructor = constructor
        self.speed = speed

    def affichage_message(self, transport_type, msg):
        return (f"Instance de {transport_type} | \ncapacité = {self.capacity}\nspeed = {self.speed}\n{msg} ")


class Car(Transport):
    def __init__(self,doors,carburant,capacity,constructor,speed):
        self.doors_numbers = doors
        self.carburant = carburant
        Transport.__init__(self,capacity=capacity, constructor=constructor, speed=speed)

    def msg(self):
        return "mon noubel affichage"

my_car = Car(5,"Fuel",5,"Ford",120)
print(my_car.speed)
print(my_car.msg())


class Plane(Transport):
    def __init__(self,company,capacity,constructor,speed):
        self.company = company
        Transport.__init__(self,capacity=capacity, constructor=constructor, speed=speed)

    def msg(self):
        msg = "mon nouvel affichage pour l'avion"
        return self.affichage_message("avion",msg)

my_plane = Plane("Airfrance",320,"Airbus",800)
print(my_plane.speed)
print(my_plane.msg())

print(type(my_plane).__name__)
#ininstance()

print(isinstance(my_plane,Car))

#issubclass
print(issubclass(Car,Plane))