from heritage import Mere
import weather_class

from music_player.music import Music



'''
Ex 1 : 
Ecrire un programme qui represente un lecteur de musique
Identifier les classes (attributs et methodes ) et les relations entre elles : (Musique, Artiste, Album, Playlist )

chaque musique a un seul artiste
Un album peut contenir des musiques de differents artistes "various artists"
chaque musique est associé à un seul album

ecrire chaque classe dans un fichier distinct
importer les classes dans un script principal pour tester les classes



'''

'''
Ex 2 : 
I. 
utiliser les fichiers json meteo_année.json pour constuire :
1- la variable X (variable explicative )
correpondant à la liste totale des valeurs des "Durée du jour"
2 - la variable y (variable expliquée )
 correpondant à liste totale des "Température moyenne"
 
3 - créer la classe LinearRegression avec la méthode fit (pour calculer theta1 et theta1)
 et predict (pour retourner la prediction de la temperature selon la durée du jour)

4 - predire la temperature pour une durée de jour donnée


Notes
utiliser le module json et "with open()" pour lire un fichier json :
weather_data = []
 with open('nom_du_fichier.json') as json_file:
        weather_data.append(json.load(json_file))
utiliser une variable qui contiendra toutes les données des fichier pour extraire les 
informations necessaires. (utiliser des boucles sur les années et les mois )


II.
la méthode fit() accepte un vecteur multidimentionel,
utiliser la methode np.c_ pour concatener la liste des "Durée du jour" et la liste des "Précipitations moyennes par jour"
utiliser cette nouvelle matrice pour lancer la méthode fit()

4 - predire la temperature pour une durée de jour et une precipitation moyenne donnée
'''