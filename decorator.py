def decorator(f):
    print("vous avez lancé la fonction "+ f.__name__)
    return f



@decorator
def ma_fonction():
    a= 2
    b= 5
    c = a+b
    return c


ma_fonction()

def generic_print(f):
    def new_function(*args,**kwargs):
        print(f'appel de la fonction avec les arguments positionnel : {args} et les arguments par mots clés : {kwargs}')
        res = f(*args,**kwargs)
        print(f'resultat = {res}')
        return res
    return new_function

def print_decorator(f):
    def new_function(a,b):
        print(f'Addition des nombres {a} et {b}')
        ret = f(a,b)
        print(f'Retourne le resultat : {ret}')
        return ret
    return new_function


def display_stars(f):
    def new_function(*args, **kwargs):
        print("***************************")
        res = f(*args, **kwargs)
        return res
    return new_function



@generic_print
def addition(a,b):
    return  a + b



@display_stars
@generic_print
def addition2(a,b,c,d):
    return a+b+c+d





print(addition(2,5))
print(addition2(2,2,5,d=10))


