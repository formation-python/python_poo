class Course:


    def activate(self):
        self.is_active = True

    def deactivate(self):
        self.is_active = False

    def display_text(self):
        print(f"Bonjour, vous avez appelé"
              f" le cours : {self.title}")

    #kwargs
    def __init__(self, **kwargs):

        '''
        self.title = dictionaire['title']
        self.is_active = dictionaire['is_active']
        self.duration = dictionaire['duration']
        self.summary = dictionaire['summary']
        self.start_date = dictionaire['start_date']
        self.end_date = dictionaire['end_date']
        self.file = dictionaire['file']
        '''

        for key, values in kwargs.items():
            setattr(self, key, values)


#passer un dictionnaire lors de l'instantiation

course_dict = {
    "title" : "Django",
    "duration" : 12,
    "is_active" : False,
    "summary" : "cours Django / Python",
    "start_date" : "2021-01-01",
    "end_date" : "2021-05-01",
    "file2" :"a new file",
    "file" : "https://files/my_python_course.pdf"
}

my_new_course = Course(**course_dict,another_attribute="un autre attribut")
print(my_new_course.title)
print(my_new_course.summary)
print(my_new_course.another_attribute)

my_other_course = Course()





