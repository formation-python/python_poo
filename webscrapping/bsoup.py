import requests
from bs4 import BeautifulSoup
import json
import pandas as pd


def scrap_page(year):
    page = requests.get(
        f'https://www.historique-meteo.net/france/rh-ne-alpes/{year}/')
    # print(page.content)
    soup = BeautifulSoup(page.content, 'html.parser')
    # print(soup)
    data_tables = soup.find_all("table", {"class": "table"})
    print(len(data_tables))

    data = []

    for i in range(len(data_tables)):
        print("*******")
        print('month '+str(i+1))
        print("*******")
        month = data_tables[i]
        lines = month.find_all("tr")
        data.append({"mois": i+1})
        for k in lines:
            key = k.find_all("td")[0].text
            value = k.find_all("td")[1].text
            data[i][key] = value
            print(key)
            print(value)
    weather_data = pd.DataFrame(data)
    print(weather_data)

    with open(f"weather_{year}.json", "w", encoding='utf8') as f:
        json.dump(data, f, indent=4, ensure_ascii=False)


def get_weather_info_by_year(year):
    url = f"https://www.historique-meteo.net/france/rh-ne-alpes/{year}/"
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    information_tables = soup.find_all("table", {"class": "table"})

    data = []
    for i in range(len(information_tables)):
        print("*********")
        data.append({"month": i + 1})
        month = information_tables[i]
        lines = month.find_all("tr")
        for line in lines:
            key = line.find_all("td")[0].text
            value = line.find_all("td")[1].text
            data[i][key] = value
    print(data)

    with open(f"meteo_{year}.json", "w", encoding='utf8') as f:
        json.dump(data, f, indent=4, ensure_ascii=False)


for i in range(2009, 2021):
    get_weather_info_by_year(i)
