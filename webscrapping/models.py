
class Course:

    def activate(self):
        self.is_active = True

    def deactivate(self):
        self.is_active = False

    def display_text(self):
        print(f"Bonjour, vous avez appelé"
              f" le cours : {self.title}")

    def __init__(self, file="", end_date="2021-03-20", start_date="2021-03-17", summary="", title="pas de titre",
                 is_active=False, duration=0):
        self.title = title
        self.is_active = is_active
        self.duration = duration
        self.summary = summary
        self.start_date = start_date
        self.end_date = end_date
        self.file = file







#instances
my_lesson = Course(title="Python", is_active=False, duration=2)
my_second_lesson = Course("Java", True, 20)

my_lesson.display_text()
my_second_lesson.display_text()

#attribut d'instance
my_lesson.title = "Python"
my_lesson.is_active = False

print(my_lesson.title)
my_second_lesson.title = "Java"
print(my_second_lesson.title)

another_lesson = Course("IA",False, 0)
print(another_lesson.title)
another_lesson.title = "IA"
print(another_lesson.title)
print(my_lesson.title)

another_lesson.display_text()
my_lesson.display_text()
my_second_lesson.display_text()


my_last_lesson = Course()
my_last_lesson.display_text()
my_second_lesson.display_text()

#

my_second_lesson.activate()
print(my_second_lesson.is_active)
my_second_lesson.deactivate()
print(my_second_lesson.is_active)

class Rectangle:
    def __init__(self, longueur=0.0, largeur=0.0, couleur="white"):
        self.longeur = longueur
        self.largeur = largeur
        self.couleur = couleur

    def calcul_surface(self):
        '''methode qui calcule la surface'''
        return self.longeur*self.largeur

    def calcul_perim(self):
        return (self.longeur+self.largeur)*2


r_1 = Rectangle(5, 3)
r_2 = Rectangle(8,2)

print(r_1.calcul_perim())
print(r_1.calcul_surface())
print(r_2.calcul_surface())
print(r_2.calcul_perim())

print(r_1.__dict__.keys())


