x = 22
print(type(x))

X = 3
print(x)
print(X)

ma_nouvelle_variable = 'this is a new one'

y = 3.14
print(type(y))

message = '''
ceci est un message
en plusieurs
lignes'''
print(message)


my_score = 9
print(my_score)

my_score = 8.9
print(my_score)

my_score = "0"
print(my_score)


# Opération
# les symboles +, -, *, /, **, //, %


x = 22 + 3
print(x)

y = 5 - 2
print(y)

z = 5 * 2
print(z)

x = 2 ** 3
print(x)

x = 5 / 2
print(type(x))

x = 5 // 2
print(x)

x = 5 % 2
print(type(x))


# Operations sur les chaines de caractères

message = "Hello"
print(message)

message2 = message + " " + "my " + " World"
print(message2)


message3 = message * 2
print(message3)


# conversion de types

message4 = message + str(2)
print(message4)

message = "22"
entier = 5
somme = int(message) + entier
print(somme)

pi = "3.14"
pi2 = float(pi)*2
print(pi2)
